<?php
	require 'autoload.php';
	require 'config.php';

	use Rest\Rest as Rest;
	use Rest\Database\SqlStatement as SqlStatement;

	$rest = new Rest();

	// /peakcompletion/api/parts/imrPartID/$imrPartID
	// /peakcompletion/api/parts/imrPartID/$imrPartID/imrPartRevisionID/$imrPartRevisionID
	// /peakcompletion/api/parts/imrPartID/$imrPartID/imrPartGroupID/$imrPartGroupID

	$rest->get(array(
		'/peakcompletion/api/parts',
		'/peakcompletion/api/parts/imrPartID/$imrPartID',
		'/peakcompletion/api/parts/imrPartGroupID/$imrPartGroupID',
		'/peakcompletion/api/parts/imrPartID/$imrPartID/imrPartRevisionID/$imrPartRevisionID',
		'/peakcompletion/api/parts/imrPartRevisionID/$imrPartRevisionID/imrPartID/$imrPartID',
		'/peakcompletion/api/parts/imrPartID/$imrPartID/imrShortDescription/$imrShortDescription',
		'/peakcompletion/api/parts/imrPartID/$imrPartID/imrLongDescription/$imrLongDescription'
	), function( $response, $database, $logger, $args ){
		$statement = new SqlStatement();
		$statement->addSelectTable('Parts');

		foreach( $args as $field => $value ){
			switch($field){
				case 'imrPartID':	
					$statement->addWhere( $field, $value );
					break;
				case 'imrPartRevisionID':
					$statement->addWhere($field, $value);
					break;
				case 'imrShortDescription':
					$statement->addWhere($field, $value);
					break;
				case 'imrLongDescription':
					$statement->addWhere($field, $value);
					break;
				case 'imrPartGroupID':
					$statement->addWhere($field, $value);
				default:
					break;
			}
		}

		$statement->prepareSelectStatement();
		echo $statement->getSelectStatement() . PHP_EOL;

		$database->select($statement->getSelectStatement(), $statement->getBindings());
		$database->execute();

		## TEMPORARY ##
		header('Control-Allow-Access-Origin: *');
		header('Content-Type: application/json');
		echo json_encode($database->getData());
		###############

		//print_r($database->getData());
		// $response->respond($database->getData());
	});

	$rest->post(array(
		'/peakcompletion/api/parts'
	), function($response, $database, $logger, $args){
		/*if(!isset($args['imrPartID'])){
			// TODO: Throw Exception
			return;
		}*/
		//print_r($args);

		$statement = new SqlStatement();
		$statement->addInsertTable('Parts');

		foreach( $args as $arg => $a ){
			echo $arg . ": " . $a . PHP_EOL;
			$statement->addInsertColumns($arg, $a);
		}

		$statement->prepareInsertStatement();
		echo $statement->getInsertStatement() . PHP_EOL;

		$database->insert($statement->getInsertStatement(), $statement->getBindings());
		$database->execute();

		/*if(!$database->execute()){
			echo "Database execute failed." . PHP_EOL;
		}*/

	});

	$rest->handle();
?>
