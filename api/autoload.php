<?php
  
  use Rest\Log\LogLevel as LogLevel;
  use Rest\Log\Logger as Logger;

  ini_set('display_errors', true); // Turn this off in production!

  function autoload( $classname ){

    if( !$classname ){
      return null; // Throw exception.
    }

    $classname = ltrim($classname, '\\');
    $filename = str_replace('\\', '/', $classname);
    $filename = str_replace('_', '/', $filename);
    $filename = $filename . '.php';

    require $filename;
  }

  spl_autoload_register('autoload');
?>
