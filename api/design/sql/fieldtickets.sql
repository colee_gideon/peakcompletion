CREATE DATABASE IF NOT EXISTS fieldticket;

USE fieldticket;

CREATE TABLE IF NOT EXISTS parts(
  partId INT UNSIGNED NOT NULL AUTO_INCREMENT,
  partNumber VARCHAR(15) NOT NULL,
  partTitle VARCHAR(45) NOT NULL,
  partDescription VARCHAR(255) NOT NULL,
  unit VARCHAR(45), /* NEEDS ATTENTION: What types?? */
  price MONEY NOT NULL,

  PRIMARY KEY (partNumber)
);

CREATE TABLE IF NOT EXISTS ticketParts(
  partId INT UNSIGNED NOT NULL,
  partNumber VARCHAR(45) NOT NULL, /* This may need to be a string if their part 'code' is alphanumerical */
  fieldTicketNumber INT UNSIGNED NOT NULL, /* */
  amount INT UNSIGNED NOT NULL,
  qtyShipped INT UNSIGNED NOT NULL,

  PRIMARY KEY (partNumber, fieldTicketNumber),
  FOREIGN KEY (partNumber) REFERENCES parts (partNumber)
);

CREATE TABLE IF NOT EXISTS customer(
  customerId INT UNSIGNED NOT NULL,     /* May be auto incrementing */
  customerName VARCHAR(45) NOT NULL,
  invoiceTo VARCHAR(45) NOT NULL,       # This field may be able to not be null as the user can specify if invoice is different than shipping
  invoiceAddress VARCHAR(45) NOT NULL,  # This field may be able to not be null as the user can specify if invoice is different than shipping
  invoiceCity VARCHAR(45) NOT NULL,     # This field may be able to not be null as the user can specify if invoice is different than shipping
  invoiceState VARCHAR(45) NOT NULL,    # This field may be able to not be null as the user can specify if invoice is different than shipping
  invoiceZipCode VARCHAR(45) NOT NULL,  # This field may be able to not be null as the user can specify if invoice is different than shipping
  /* customerAFENumber,                    NEEDS ATTENTION: WHAT TYPES? */
  /* customerREQNumber,                    NEEDS ATTENTION: WHAT TYPES? */
  phoneNumber VARCHAR(8) NOT NULL,                          /* NEEDS ATTENTION: Possibly string? */
  phoneAreaCode VARCHAR(3) NOT NULL,                        /* NEEDS ATTENTION: Possibly string? */
  shippingAddress VARCHAR(45) NOT NULL,
  shippingCity VARCHAR(45) NOT NULL,
  shippingState VARCHAR(45) NOT NULL,
  shippingZipCode VARCHAR(45) NOT NULL,

  PRIMARY KEY (customerId)
);

CREATE TABLE IF NOT EXISTS well(
  wellId INT UNSIGNED NOT NULL,
  customerId INT NOT NULL,
  wellName VARCHAR(45) NOT NULL,
  customerName VARCHAR(45) NOT NULL,
  wellLocation VARCHAR(45) NOT NULL,
  rigUnit VARCHAR(45) NOT NULL,

  PRIMARY KEY (wellId)
);

CREATE TABLE IF NOT EXISTS fieldticket(

);
