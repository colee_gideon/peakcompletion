DROP DATABASE IF EXISTS pc_test;

CREATE DATABASE pc_test;

USE pc_test;

CREATE TABLE fieldticket(
  fieldTicketId INT UNSIGNED NOT NULL AUTO_INCREMENT,
  customerName VARCHAR(45) NOT NULL,
  fieldTicketDate VARCHAR(10) NOT NULL,

  PRIMARY KEY(fieldTicketId)
);

INSERT INTO fieldticket (fieldticketid, customerName, fieldTicketDate) VALUES (0, "Customer 1", "03-03-2016"), (0, "Customer 2", "03-01-2016"), (0, "Customer 3", "08-31-2014"), (0, "Customer 4", "06-15-2025")
