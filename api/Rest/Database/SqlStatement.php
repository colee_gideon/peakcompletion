<?php

	namespace Rest\Database;

	class SqlStatement {

		/*
			target: imrPartID
					imrPartRevisionID
					imrShortDescription
					imrLongDescription
			table: table to access
			where: 
				field : value
			top:
			
			SELECT [TOP $top][$target, *] FROM [$table] [WHERE $field = $value];
		*/
		private $select = array();
		private $insert = array();
		private $where 	= array();
		private $bind = array();
		private $select_statement = null;
		private $insert_statement = null;

		public function __construct(){
		}

		// INSERT INTO $table [$fields] VALUES (:$fields)
		public function addInsertTable( $table ){
			$this->insert['table'] = $table;
		}

		public function addInsertColumns( $column, $value ){
			$this->insert['columns'][$column] = $value;
		}

		public function prepareInsertStatement(){
			$statement = 'INSERT INTO';

			if( !isset($this->insert['table'])){
				// TODO: Throw Exception
				return;
			}else{
				$statement = preg_replace('/([\S\s]+)/',' $1 ' . $this->insert['table'], $statement);
			}

			if( !isset($this->insert['columns'])){
				return;
			}else{
				$columns_string = '(' . implode(', ', array_keys($this->insert['columns'])) . ')';
				$values_string = '(' . preg_replace('/([\S]+)/', ':$1', implode(', ', $this->insert['columns'])) . ')';

				foreach( $this->insert['columns'] as $column => $value ){
					$this->bind[':' . $column] = $value;
				}

				$statement = preg_replace('/([\S\s,]+)/','$1 ' . $columns_string . ' VALUES ' . preg_replace('/([^(),\s][\S]+)/', ':$1', $columns_string) . ';',$statement);
			}

			$this->insert_statement = $statement;
		}

		public function getInsertStatement(){
			return $this->insert_statement;
		}

		public function addSelectTable($table){
			$this->select['table'] = $table;
		}

		public function addSelectTarget( $target ){
			$this->select['target'][$target] = $target;
		}

		public function addWhere( $field, $value ){
			$this->select['where'][$field] = $value;
		}

		public function addTop( $top ){
			$this->select['top'] = $top;
		}

		public function getSelectStatement(){
			return $this->select_statement;
		}

		public function getBindings(){
			return $this->bind;
		}

		public function prepareSelectStatement(){
			$statement = 'SELECT *';

			if(array_key_exists('top', $this->select)){
				$statement = preg_replace('/SELECT[ ]([\w*]+)/', 'SELECT TOP ' . $this->select['top'] . ' $1', $statement);
			}

			if(array_key_exists('target', $this->select)){
				$target_implode = implode(', ', $this->select['target']);
				$statement = preg_replace('/\*/', $target_implode, $statement);
			}

			if(!array_key_exists('table', $this->select)){
				// TODO: Log and Throw exception
				return;
			}else{
				$statement = preg_replace('/([\S\s*]+)/', '$1 FROM ' . $this->select['table'], $statement);
			}

			if(array_key_exists('where', $this->select)){

				$where = '';
				$count = 1;
				$max = count($this->select['where']);

				foreach($this->select['where'] as $field => $value){
					$where .= $field . ' = :' . $field;
					if( $count < $max ){
						$where .= ' AND ';
					}
					$this->bind[':'. $field] = $value;
					$count++;
				}

				$statement = preg_replace('/([\S\s*]+)/', '$1 WHERE ' . $where, $statement);
			}

			$this->select_statement = $statement . ';';
		}
	}
?>