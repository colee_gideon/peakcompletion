<?php
	namespace Rest\Database;
	use Rest\Interfaces\Database\DatabaseInterface;
	//require_once INTERFACE_PATH . 'databaseinterface.php';

	class Database implements DatabaseInterface {

		private $pdo = null;
		private $pdostatement = null;

    	public function __construct(){
    		$this->pdo = new \PDO(DB_DSN, DB_USERNAME, DB_PASSWORD);
    	}

    	public function connect(){}

  		public function execute(){
  			if(!$this->pdostatement->execute()){
  				print_r($this->pdostatement->errorInfo());
  			}
  			//$arr = $this->pdostatement->errorInfo();
  			//print_r($arr);
  		}

  		public function getData(){
  			return $this->pdostatement->fetchAll(\PDO::FETCH_ASSOC);
  		}

    	public function select( $statement, $bindings ){
    		$this->pdostatement = $this->pdo->prepare($statement);
    		foreach( $bindings as $binding => $value ){
    			$this->pdostatement->bindValue($binding, $value);
    		}
    	}

    	public function insert( $statement, $bindings ){
    		$this->pdostatement = $this->pdo->prepare($statement);
    		foreach( $bindings as $binding => $value ){
    			echo $binding . " " . $value . PHP_EOL;
    			$this->pdostatement->bindValue($binding, $value);
    		}
    	}

    	public function update( $statement, $bindings ){

    	}



  	}
  
?>
