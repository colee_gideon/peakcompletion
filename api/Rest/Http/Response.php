<?php

  namespace Rest\Http;
  
  class Response {

    private static $formats = array(
      'json' => 'application/json',
      'xml' => 'application/xml'
    );

    /*
      A list of headers to be sent out with the request.
    */
    protected $headers = array();
    protected $data = array();

    public function __construct( ){
      
    }

    protected function setHeader( $header, $replace = null, $code = null ){
      $this->headers[] = $header;
    }

    protected function listHeaders(){
      foreach( $this->headers as $header ){
        echo $header . "\n";
      }
    }

    public static function buildResponse( $data, $format ){
      if( $format == self::$formats['json'] ){
        return json_encode();
      }
    }

    public static function sendFnf(){
      header('HTTP/1.1 404', TRUE, 404);
      echo "File not found.";
    }


    protected function executeHeaders(){

    }
  }
?>
