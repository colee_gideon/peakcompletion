<?php
	namespace Rest\Http;

    use Rest\Interfaces\Http\MessageInterface;

	abstract class Message implements MessageInterface {

		protected $version = '1.1';

		private $headers = array();

		private $protocolVersions = array(
			'HTTP/2.0' => '2.0',
			'HTTP/1.1' => '1.1',
			'HTTP/1.0' => '1.0'
		);

		public function getProtocolVersion(){
            return $this->protocolVersions[$_SERVER['SERVER_REQUEST']];
		}

  		public function withProtocolVersion($version){
  		    $clone = clone $this();

  			$clone->$version = $version;
  			return $clone;
  		}
        
  		public function getHeaders(){

  		}

  		public function hasHeader($name){

  		}

  		public function getHeader($name){

  		}

  		public function getHeaderLine($name){

  		}

  		public function withHeader($name,$value){

  		}

  		public function withAddedHeader($name, $value){

  		}

  		public function withoutHeader($name){

  		}

  		public function getBody(){
            return json_decode(file_get_contents('php://input'));
  		}

        public function withBody( /* StreamInterface $body*/ ){
            //return fopen('php://input');
        }
	}
?>