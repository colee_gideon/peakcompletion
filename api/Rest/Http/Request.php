<?php
    namespace Rest\Http;
    use Rest\Interfaces\Http\RequestInterface;
    class Request extends Message implements RequestInterface {
        
        const POST  = 'POST';
        const GET   = 'GET';
        const PUT   = 'PUT';

        private $method = null;
        private $uri = null;

        public function __construct(){
            $this->method = $_SERVER['REQUEST_METHOD'];
            $this->uri = $_SERVER['REQUEST_URI'];
        }

        public function getMethod(){
            return $this->method;
        }

        public function getUri(){
            return $this->uri;
        }
    }
?>
