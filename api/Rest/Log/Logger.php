<?php
	namespace Rest\Log;

	use Rest\Interfaces\Log\LoggerInterface;
	use Rest\Log\LogLevel as LogLevel;

	class Logger implements LoggerInterface {

		public function debug($message, array $context = array()){
			$this->log( LogLevel::DEBUG, $message, $context );
		}

		public function info($message, array $context = array()){
			$this->log( LogLevel::INFO, $message, $context );
		}

        public function notice($message, array $context = array()){}

        public function warning($message, array $context = array()){}

        public function error($message, array $context = array()){}

        public function critical($message, array $context = array()){}

        public function alert($message, array $context = array()){}

        public function emergency($message, array $context = array()){}

		public function log( LogLevel $level, $message, array $context = array() ){
			echo $level . ': ' . $message . PHP_EOL;
		}
	}
?>