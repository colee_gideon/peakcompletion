<?php

  namespace Rest\Model;
  class Customers {

    /*
      Property: customer_id
      The customer id associated with the table Customer.
    */
    private $customer_id = null;

    /*
      Property: customer_name
      The title of the company.
    */
    private $customer_name = null;

    /*
      Property: invoice_to
    */
    private $invoice_to = null;

    /*
      Property: invoice_address
    */
    private $invoice_address = null;

    /*
      Property: invoice_city
    */
    private $invoice_city = null;

    /*
      Property: invoice_state
    */
    private $invoice_state = null;

    /*
      Property: invoice_zipcode
    */
    private $invoice_zipcode = null;

    /*
      Property: customer_afe
    */
    private $customer_afe = null;

    /*
      Property: customer_req
    */
    private $customer_req = null;

    /*
      Property: no
    */
    private $no = null;

    /*
      Property: area_code
    */
    private $area_code = null;

    /*
      Property: shipping_address
    */
    private $shipping_address = null;

    /*
      Property: shipping_city
    */
    private $shipping_city = null;

    /*
      Property: shipping_state
    */
    private $shipping_state = null;

    /*
      Property: shipping_zipcode
    */
    private $shipping_zipcode = null;


    public function __construct( ){
			echo "Customers Constructor.";
		}

    /**
    * @GET /peakcompletion/api/customers
    */
    public function getCustomers(){

    }
  }
?>
