<?php

    namespace Rest\Model;



    class Parts extends Resource {

        private $sqlItems = array();
        private DatabaseInterface $database = null;

        public function __construct( /*DatabaseInterface $database, MediaInterface $media = null*/ ){
        
        }


        // GET
    
        /**
        * @GET /peakcompletion/api/parts/imrPartID/$imrPartID
        * @GET /peakcompletion/api/parts/imrPartID/$imrPartID/imrPartRevisionID/$imrPartRevisionID
        * @GET /peakcompletion/api/parts/imrPartID/$imrPartID/imrShortDescription/$imrShortDescription
        * @GET /peakcompletion/api/parts/imrPartID/$imrPartID/imrLongDescription/$imrLongDescription
        */
        public function getFieldByPartId( $imrPartID, $imrPartRevisionID = null, $imrShortDescription = null, $imrLongDescription = null){
        
        }

        /**
        * @GET /peakcompletion/api/parts
        * @GET /peakcompletion/api/parts/imrPartRevisionID/$imrPartRevisionID
        * @GET /peakcompletion/api/parts/imrShortDescription/$imrShortDescription
        * @GET /peakcompletion/api/parts/imrLongDescription/$imrLongDescription
        * @GET /peakcompletion/api/parts/imrPartGroupID/$imrPartGroupID
        */
        public function getPartsByField( $imrPartRevisionID = null, $imrShortDescription = null, $imrLongDescription = null, $imrPartGroupID = null){
            // Return all parts
        }

        /**
        * @GET /peakcompletion/api/parts
        */
        public function getAllParts(){

        }

        // POST
        /**
        * @POST /peakcompletion/api/parts
        */
        public function postParts(){

        }

        // PUT
    

    

    


    public function getPartWith( $id = null, $field = null ){

    }
  }
?>
