<?php

  namespace Rest\Model;

  use Rest\Interfaces\Database\DatabaseInterface;

  abstract class Resource {
  	public function __construct();
  	public function addSqlItem( $item );
  	public function deleteSqlItem( $item );
  }
?>
