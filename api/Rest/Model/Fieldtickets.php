<?php
	namespace Rest\Model;
	/*
		Model for the Field Ticket Resource
	*/
	class Fieldtickets {
		/*
			Property: id
			Id that belongs to the field ticket
		*/
		private $id = null;

		/*
			Property: shippedTo
		*/
		private $shippedTo = null;
		private $shippingAddress = null;
		private $shippingCity = null;
		private $shippingState = null;
		private $shippingZipCode = null;
		private $careOf = null;
		private $invoiceTo = null;
		private $invoiceAddress = null;
		private $invoiceCity = null;
		private $invoiceState = null;
		private $invoiceZipCode = null;
		private $attention = null;
		private $customerAFE = null;
		private $customerREQ = null;
		private $filledBy = null;
		private $stockPoint = null;
		private $wellName = null;
		private $wellLocation = null;
		private $rigUnit = null;
		private $orderedBy = null;
		private $dateOrdered = null;
		private $dataFilled = null;
		private $areaCode = null;
		private $no = null;
		private $ticketParts = array();

		public function __construct( ){
		}
	}
?>
