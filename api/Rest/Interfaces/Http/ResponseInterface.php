<?php
	namespace Rest\Interfaces\Http;
	interface ResponseInterface extends MessageInterface {
		public function getStatusCode();
		public function withStatus( $code, $reasonPhrase = '' );
		public function getReasonPhrase();
	}
?>
