<?php
	namespace Rest\Interfaces\Http;
  	interface RequestInterface extends MessageInterface {
  		//public function getRequestTarget();
  		//public function withRequestTarget();
  		public function getMethod();
  		//public function withMethod($method);
  		public function getUri();
  		//public function withUri(UriInterface $uri, $preserveHost = false);
  	}
?>
