<?php
	namespace Rest\Interfaces\Database;
  	interface DatabaseInterface {
  		public function connect();
  		public function execute();
  		public function select($statement, $bindings);
  		public function insert($statement, $bindings);
  		public function update($statement, $bindings);
  	}
  
?>
