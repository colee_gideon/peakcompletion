<?php
    namespace Rest;

    use Rest\Interfaces\Http\RequestInterface;
    use Rest\Interfaces\Http\ResponseInterface;
    use Rest\Interfaces\Database\DatabaseInterface;
    //use Rest\Interfaces\Log\LoggerInterface;

    use Rest\Log\LogLevel as LogLevel;
    use Rest\Log\Logger as Logger;
    use Rest\Http\Request as Request;
    use Rest\Http\Response as Response;
    use Rest\Database\Database as Database;

    class Rest {

        private $request = null;
        private $response = null;
        private $database = null;
        private $logger = null;
        private $map = array();
        private $args = array();

        public function __construct(DatabaseInterface $database = null, RequestInterface $request = null,ResponseInterface $response = null ){
            //$this->logger = new Logger();
            $this->database = isset($database) ? $database : new Database();
            $this->request = isset($request) ? $request : new Request();
            $this->response = isset($response) ? $response : new Response();
        }

        /*
        * Function: setRequestInterface
        *
        * @param $request The request interface to set
        */
        public function setRequest( RequestInterface $request ){
            $this->request = $request;
        }

        /*
        * Function: getRequest
        *
        *
        */
        public function getRequest(){
            return $this->request;
        }

        /*
        * Function: setResponseInterface
        *
        * @param $response The response interface to set
        */
        public function setResponse( ResponseInterface $response ){
            $this->response = $response;
        }

        /*
        * Function: getResponse
        *
        * 
        */
        public function getResponse(){
            return $this->response;
        }

        /*
        * Function: setDatabaseInterface
        *
        * @param $database The database interface to set
        */
        public function setDatabase( DatabaseInterface $database ){
            $this->database = $database;
        }

        /*
        * Function: getDatabase
        *
        *
        */
        public function getDatabase(){
            return $this->database;
        }

        /*
        * Function: createMap
        *
        * @param $method The HTTP method to map to
        * @param $urls The array of urls associates with this 
        *   map.
        * @param $function The callback function associated with 
        *   this map.
        */
        private function createMap( $method, $urls, $function ){
            if(!$urls){
                echo 'Log Error and throw no URI found exception.</br>';
                // TODO: Log Error
                return; // TODO: Throw no URI found exception
            }else if(!$function){
                echo 'Log Error and throw no function found exception.</br>';
                // TODO: Log Error
                return; // TODO: Throw no function found exception.
            }

            foreach( $urls as $url ){
                // TODO: Log that a map is being created.
                $this->map[$method][$url] = $function;
            }
        }

        /*
        * Function: put
        *
        * @param $urls The urls to associate with this
        *   HTTP request.
        * @param $function The function to execute associated with 
        *   this HTTP request.
        */
        public function put( $urls, $function ){
           
            $this->createMap('PUT', $urls, $function);
        }

        /*
        * Function: post
        *
        * @param $urls The urls to associate with this
        *   HTTP request.
        * @param $function The function to execute associated with 
        *   this HTTP request.
        */
        public function post( $urls, $function ){
            $this->createMap('POST', $urls, $function);
        }

        /*
        * Function: get
        *
        * @param $urls The urls to associate with this
        *   HTTP request.
        * @param $function The function to execute associated with 
        *   this HTTP request.
        */
        public function get($urls, $function){
            //$this->logger->debug('GETTING');
            $this->createMap('GET', $urls, $function);
        }

        /*
        * Function: findMap
        *
        * @param $method 
        * @param $uri
        */
        private function findMap( $method, $uri ){
            $regex_capture = '/\$([\w\d\\\|\+\(\)#-]+)/';
            $backreference = '(?P<$1>[^\/]+)';

            $urls = $this->map[$method];

            foreach( $urls as $url => $function ){
                $regex = preg_replace($regex_capture, $backreference, $url);
                $args = array();
                if( preg_match(":^$regex$:", urldecode($uri), $matches) ){
                    foreach( $matches as $endpoint => $value ){
                        if(is_int($endpoint)) continue;
                        $args[$endpoint] = $value;
                    }

                    return array($function, $args);
                }
            }
        }

        /*
        * Function: handle
        *
        *
        */
        public function handle(){
            $method = $this->request->getMethod();
            $uri = $this->request->getUri();
            list($function, $args) = $this->findMap($method, $uri);

            if(!$function){
                // TODO: Log Error
                // TODO: Throw exception
                return;
            }

            switch($method){
                case 'POST':
                case 'PUT':
                    if($body = $this->request->getBody()){
                        foreach( $body as $index => $value ){
                            $this->args[$index] = $value;
                        }

                        $function($this->response, $this->database, $this->logger, $this->args);
                    }else{
                        $this->args = $args;
                        $function($this->response, $this->database, $this->logger, $this->args);
                    }
                    break;
                case 'GET':
                    $this->args = $args;
                    $function($this->response, $this->database, $this->logger, $this->args);
                    break;
                default:
                    // TODO: Throw unknown HTTP method exception.
                    break;
            }
        }
    }

?>
