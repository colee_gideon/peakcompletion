# Peak Completion Back-end Server API #

### What is this repository for? ###

This repository is used to retrieve, create, and modify data in the database.
The server uses the REST architectural style to retrieve, create, and modify data
using HTTP verbs, resources, and endpoints.

### How to send requests to the server ###
Example URI: http://server_location/peakcompletion/api/<resource>/<resource_identifier>/<endpoint>/<endpoint_identifier>

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
